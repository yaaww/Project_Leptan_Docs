Assembly
========

Parts list
----------

.. list-table:: Part list
   :widths: 10 50
   :header-rows: 1

   * - Qty.
     - Description
   * - 18
     - Dynamixel AX-12A
   * - 1
     - Dynamixel U2D2 Servo Interface
   * - 1
     - ROBOTIS Robot Cable-3P 140mm 10 pcs
   * - 1
     - ROBOTIS Robot Cable-3P 180mm 10 pcs
   * - 1
     - ROBOTIS Robot Cable-3P 200mm 10 pcs
   * - 1
     - ROBOTIS P04-F2 10pcs
   * - 1
     - ROBOTIS P04-F3 10pcs
   * - 2
     - ROBOTIS BPF-WA/BU 10pcs
   * - 10
     - MOLEX 22-03-5035

I have ordered them at `Mouser <https://mouser.com/>`_ due to price and availability.

.. warning::
  The AX-12A Bulk packs (6 Pack) ships without any additional parts, which is the recommended way
  which matches these tables.
  If you order the AX-12A in single pack they ship with P04-F2, P04-F3, BPF-WA/BU each and screws.
  So these parts don't have to be ordered.
  I would recommend ordering the 6 pack because it costs less and you don't have that many spare
  parts at the end.


Screws
------

The required screws per leg are:

* 36pcs M2 x 6mm Screws
* 3pcs M3 x 10mm
* 16pcs M2 Nuts

.. list-table:: Leg screws
   :widths: 10 50
   :header-rows: 1

   * - Qty.
     - Description
   * - 216
     - M2 x 6mm
   * - 96
     - M2 Nut
   * - 18
     - M3 x 10mm

The body requires some additional screws:

* 4pcs M2 x 16mm for the Led Ring Bar or
* 4pcs M2 x 12mm for the straight bar
* 8pcs M2 x 6mm for the Lower body top
* 4pcs M2 x 10mm for mounting the Raspberry Pi
* 8pcs M2 x 6mm to mount each leg to the body
* 8pcs M2 Nuts to mount each leg to the body

.. list-table:: Body screws
   :widths: 10 50
   :header-rows: 1

   * - Qty.
     - Description
   * - 4
     - M2 x 16mm/12mm
   * - 56
     - M2 x 6mm
   * - 4
     - M2 x 10mm
   * - 48
     - M2 Nut


In Summary this results in:

.. list-table:: Total screws
   :widths: 10 50
   :header-rows: 1

   * - Qty.
     - Description
   * - 272
     - M2 x 6mm
   * - 144
     - M2 Nut
   * - 18
     - M3 x 10mm
   * - 4
     - M2 x 10mm
   * - 4
     - M2 x 16mm/12mm


Assembly
---------


Body
~~~~

The parts are printed with PETG, no supports required. The printable files can be found at
`Prusa Printers. <https://www.prusaprinters.org/prints/71601-lepta-v10>`_

First the Battery is placed in the lower body:

.. image:: ../Pictures/Body_01.jpg
  :align: center

The battery cage top is screwed on top of the battery. The 4 holes with a small standoff are
intended for the raspberry and therefore left without a screw for now.

.. image:: ../Pictures/Body_02.jpg
  :align: center

Then the powered Molex connectors are screwed into place:

.. image:: ../Pictures/Body_03.jpg
  :align: center


Also you can screw on the U2D2 to the standoff at the front of the robot.
This concludes the body assembly for the moment.

Before the upper part of the body can be assembled the legs have to be done.

Legs
~~~~

A completed leg looks like this:

.. image:: ../Pictures/Leg_02.jpg
  :align: center

The coxa is assembled by connecting the P04-F3 and the P04-F2 like this using 4pcs M2x6mm screws
with 4pcs M2 nuts:

.. image:: ../Pictures/Coxa_01.jpg
  :width: 300
  :align: center


The femur is simply connected by using 4pcs M2x6mm screws directly into the plastic without a Nut.
Wherever a servo is connected be sure to use the BPF-WA/BU on the opposite side than the horn.

Exploded view of a single leg:

.. image:: ../Pictures/Leg_01.jpg
  :align: center

Be sure to keep the orientation of the servos constant, otherwise you would have to change the
control.


Servo Numbering
~~~~~~~~~~~~~~~

Each Servo requires an unique ID in order to be addressed by the system. This ID can be assigned
using the `Dynamixel Wizard. <https://emanual.robotis.com/docs/en/software/dynamixel/dynamixel_wizard2/>`_

.. image:: ../Pictures/Leg_Servo_Names.png
  :align: center

Connect each Servo individually to your PC using the serial interface, and the assign these IDs:

.. list-table::
   :widths: 20 10 10 10
   :header-rows: 1

   * - Leg ID
     - Coxa
     - Femur
     - Tibia
   * - Right Rear
     - 8
     - 10
     - 12
   * - Right Middle
     - 14
     - 16
     - 18
   * - Right Front
     - 2
     - 4
     - 6
   * - Left Rear
     - 9
     - 11
     - 13
   * - Left Middle
     - 15
     - 17
     - 19
   * - Left Front
     - 3
     - 5
     - 7

Connect the servo motors on each leg together, from femur to tibia to coxa.


Body 2
~~~~~~~

After all 6 legs have been assembled they can be attached to the lower body, by using 4 screws with
Nuts.

Connect the 6 Coxa motors with the Molex connectors (3 on each side).

Now screw the raspberry pi into place, connect the cables.

Attach the top body plate to the robot. Now is the time to finish the cabling.

.. image:: ../Pictures/BodyCabling_01.jpg
  :align: center


The battery is connected to the DC/DC converter, which supplies the raspberry.


Cabling
~~~~~~~

Here is a rough connection diagram of Leptan:

.. image:: ../Pictures/Cabling_01.png
  :align: center

The DC/DC converter should supply enough current for the raspberry pi, it is recommended to provide
3A.


Supervisor Cabling
~~~~~~~~~~~~~~~~~~

These parts are not mandatory for the robot, but can be a big help. I would recommend to use at
least the INA260 or a similar device to supervise the Battery.

.. image:: ../Pictures/Cabling_02.png
  :align: center

**Fan:**

* GPIO 18 PWM0

**LED Ring:**

* GPIO 10 SPI MOSI

**Voltage monitor INA260:**

* GPIO 2 I2C Data
* GPIO 3 I2C Clock


Upper Body
~~~~~~~~~~

After the cabling is done the upper part of the body can be screwed on top, including either the
straight bridge or the bridge including the Fan and LED ring, depending on your choice of Hardware.

The function of the bridge is to make the body more rigid, while allowing easy access to the
electronics.

The straight bridge can simply be screwed on top of the body, while the bridge containing the LED
ring and fan requires some assembly.

1. Push the LED ring and the Fan inside the ring:

.. image:: ../Pictures/LedRing_01.jpg
  :align: center

2. Solder the wires to the LED ring through the hole in the back:

.. image:: ../Pictures/LedRing_02.jpg
  :align: center

3. Snap the RingBar_Spacer to the LED ring. The spacer is required because otherwise the Ring would
be obstructed by the USB ports of the Raspberry Pi:

.. image:: ../Pictures/LedRing_03.jpg
  :align: center

4. Push the Ring cover on top of the Ring, no glue required as friction keeps everything in place.
Don't forget to connect the LED ring and the Fan to the Raspberry Pi.
Screw the RingBar on top of the robot. The result should look something like this:

.. image:: ../Pictures/LedRing_04.jpg
  :align: center



And with this, the assembly of Lepta is finished.
