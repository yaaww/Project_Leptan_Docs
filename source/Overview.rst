Overview
========

Motivation
----------

I started the project to learn some more about robotics, and because I always thought these walking robots looked very cool and I wanted one. And since it is much cooler to build the robot yourself than to simply buy it, i started the project.
Also i wanted a platform to test some machine learning algorithms and other stuff, but i am not there yet. A walking robot can test algorithms and new stuff without the risk of a devastating crash like a drone would suffer.

Status of the Project
---------------------

Lepta can walk straight, turn, walk in circles or move its body without moving the feet.
The robot is controlled using a Bluetooth joy pad, as time of writing only the XBOX One joy pad has been configured.

Lepta can perform the following actions as of time of writing:

- Stand up/Sit down
- Rotate the body without moving the feet (prove of inverse kinematics)
- Walking straight in arbitrary direction with arbitrary speed
- Turning around arbitrary point with arbitrary speed

Naming
------

Lepta or Leptan is an abbreviation of Leptanillinae, which is a subfamily of ants. `Wikipedia <https://en.wikipedia.org/wiki/Leptanillinae>`_

Hexapods are 6 legged robots, resembling ants in many ways. Also Lepta/Leptan was quite free when performing web searches or regarding domains, preventing confusions in the future.

Also Lepta is female, because male ants are called Drones and a Hexapod is not a Drone.


How Does it Walk?
-----------------

The controller updates the position of each foot regularly in order to achieve a gait.

Gait
~~~~

Gait generally consists of two phases, the swing and the stance phase.
Swing phase is when the foot is in the air, swinging forward.
Stance phase is when the food is on the ground, moving backwards pushing the body forward.

.. image:: Pictures/Gait_Phases_01.gif
  :align: center

In bipedal creatures one foot is in stance phase while the others are in swing phase.

With multi legged creatures there are more options. A Hexapod can for example have 3 legs in stance while the other 3 are in the swing phase.
So for example the left front leg is in stance while the right front leg is in swing, then the left middle is in swing and so on.

This is called a tripod gait and is commonly used by ants and other 6 legged animals.

.. image:: Pictures/Tripod_Gait_01.gif
  :align: center


Inverse Kinematics
~~~~~~~~~~~~~~~~~~

Now that we have determined the motion of each foot, the Raspberry has to control each servo in order for the foot to reach its destination.
This is done using inverse kinematics, basically you input the coordinates of a foot and it outputs the required angle for each joint.

These angles are than communicated to each servo.
