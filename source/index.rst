Welcome to Project Leptan's Documentation!
===============================================

.. image:: Pictures/Lepta_01.jpg
  :align: center

Project Leptan is a fully open source, 3D printed hexapod robot, based on the open source Robot Operationg System ROS2.

.. image:: Pictures/Lepta_walking_1.gif
  :align: center

The full code can be found on `Gitlab. <https://gitlab.com/Combinatrix/project-leptan/>`_

The hardware consist of a Rasapberry Pi 4 for as its brain, 18 pcs Dynamixel AX-12A digital Servo
motors together with a U2D2. The frame and legs are 3D printed, assembled with a bunch of screws.
Other parts include a 3S LiPo battery, a DC/DC converter to supply the Raspberry and some cabling.
Optional Hardware used for better control is an LED ring as status & power indicator and a
Voltage/Current meter for battery supervision.

Control of the robot is done using a joypad, right now an XBOX One Bluetooth joypad, which can be
directly connected to the Raspberry.

There is also a visualization (RVIZ) which you can run on your PC without the physical robot. A
proper simulation (Gazebo) is not yet implemented, but will come in the future.

The code is mostly written in C/C++, while the supervisor is written in python. This is due to the availability of
python libraries and in order to also write a python node for diversity.


Next Steps
==========

These are just a few things I intend to implement in the future

Hardware
--------

- Removable Battery (Make it easier)
- Relay to turn off the Servos completely
- Smaller DC/DC converter
- Sensors
  - LIDAR
  - IMU
  - Camera
- ...

Software
--------

- Smooth leg motion
- Adjustable leg step height
- Further optimize gait
- Different gait styles
- ...


Contents
========

.. toctree::
   :maxdepth: 3

   Overview
   Installation
   Code
   Hardware
