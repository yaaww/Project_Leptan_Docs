.. _InstallationGuide:

Installation
============

Options for installing Leptan:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Installation/PC_setup
   Installation/RPi_setup
