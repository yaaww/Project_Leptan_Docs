Raspberry Pi Setup
==================


Setup
------


Install Ubuntu Server 20.04 LTS on Raspberry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Download the image `ubuntu.com <https://ubuntu.com/download/raspberry-pi>`_
2. `Installation instruction <https://www.raspberrypi.org/documentation/installation/installing-images/>`_


Clone the Git repo
~~~~~~~~~~~~~~~~~~

If an ssh key is required, you first need to copy the private key to your .ssh folder, and then execute these commands:

start the ssh server
~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  eval `ssh-agent -s`

Add your key to the list
~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  ssh-add .ssh/<your_key_name>

Authenticate to gitlab
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  ssh -T git@gitlab.com

Clone the repo
~~~~~~~~~~~~~~
.. code-block:: bash

  git clone --recurse-submodules https://gitlab.com/Combinatrix/project-leptan.git


Install ROS2 Foxy on the Raspberry
----------------------------------
Instructions at `roboticsbackend.com <https://roboticsbackend.com/install-ros2-on-raspberry-pi/>`_
or `ROS2 homepage. <https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html>`_


Setup locale
~~~~~~~~~~~~
.. code-block:: bash

  sudo locale-gen en_US en_US.UTF-8
  sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
  export LANG=en_US.UTF-8

Setup sources
~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt update && sudo apt install curl gnupg2 lsb-release
  sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

Install ROS2
~~~~~~~~~~~~
.. code-block:: bash

  sudo apt update
  sudo apt install ros-foxy-ros-base

Source ROS files
~~~~~~~~~~~~~~~~

Add this to the end of the .bashrc file
.. code-block:: bash

  source /opt/ros/foxy/setup.bash
  source /home/ubuntu/project-leptan/Software/install/setup.bash


Install Colcon
~~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt install python3-colcon-common-extensions


Install CPP Compiler
~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt install g++



Supervisor Requirements
-----------------------


Install Pip3 if not present yet:
.. code-block:: bash

  sudo apt install python3-pip


WS2812 LEDs
~~~~~~~~~~~

Install the neopixel SPI driver for the LED ring::

  sudo pip3 install adafruit-circuitpython-neopixel-spi

Pin GPIO 10 which is SPI 0 MOSI

SPI
~~~
Instructions can be found at `SPI <https://forum.up-community.org/discussion/2141/solved-tutorial-gpio-i2c-spi-access-without-root-permissions>`_
and `Neopoixel <https://learn.adafruit.com/circuitpython-neopixels-using-spi>`_

For SPI-bus access create the file **/etc/udev/rules.d/50-spi.rules** with the following contents::

  SUBSYSTEM=="spidev", GROUP="spiuser", MODE="0660"

requires reboot or reloading the udev rules.

Next, copy and paste or type these lines into the terminal as the user you want to give access to the bus::

  sudo groupadd spiuser
  sudo adduser "$USER" spiuser


INA260
~~~~~~
.. code-block:: bash

  pip3 install adafruit-circuitpython-ina260

For I2C-bus access create the file **/etc/udev/rules.d/50-i2c.rules** with the following contents::

  SUBSYSTEM=="i2c-dev", GROUP="i2cuser", MODE="0660"

requires reboot or reloading the udev rules.

::

  sudo groupadd i2cuser
  sudo adduser "$USER" i2cuser


Temperature
~~~~~~~~~~~
.. code-block:: bash

  pip3 install gpiozero

For GPIO access without root create the file **/etc/udev/rules.d/50-gpio.rules** with the following contents::

  SUBSYSTEM=="bcm2835-gpiomem", GROUP="gpio", MODE="0660"
  SUBSYSTEM=="gpio", GROUP="gpio", MODE="0660"
  SUBSYSTEM=="gpio*", PROGRAM="/bin/sh -c '\
          chown -R root:gpio /sys/class/gpio && chmod -R 770 /sys/class/gpio;\
          chown -R root:gpio /sys/devices/virtual/gpio && chmod -R 770 /sys/devices/virtual/gpio;\
          chown -R root:gpio /sys$devpath && chmod -R 770 /sys$devpath\
  '"


.. code-block:: bash

  sudo groupadd gpiouser
  sudo adduser "$USER" gpiouser

Enable shutdown
~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo chmod u+s /sbin/shutdown


Eanble Linux event interface in Python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  pip3 install evdev


Setup Joypad
------------

Instruction can be found `pimylifeup.com <https://pimylifeup.com/xbox-controllers-raspberry-pi/>`_

Install required Software for XBOX controller::

  sudo apt install xboxdrv

This command disables the Enhanced Re-Transmission Mode (ERTM) of the Bluetooth module, with it,
enabled the Xbox Controller won’t pair correctly::

  echo 'options bluetooth disable_ertm=Y' | sudo tee -a /etc/modprobe.d/bluetooth.conf

Install Bluetooth
~~~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt install pi-bluetooth

Follow a guide like `this one <https://pimylifeup.com/xbox-controllers-raspberry-pi/>`_ in order to
pair & connect the controller.

Add the user to the input group in order to access the joypad:

.. code-block:: bash

  sudo adduser "$USER" input

Further Requirements
~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt install ros-foxy-joy


Run the Supervisor
------------------

The following command starts the supervisor:

.. code-block:: bash

  ros2 run supervisor supervisor_node

Start at boot
~~~~~~~~~~~~~

Only thing left is to auto-start the supervisor at boot. This is done using Crontab.

Open the crontab file:

.. code-block:: bash

  crontab -e

Add this line:

.. code-block:: bash

  @reboot bash -ic "ros2 run supervisor supervisor_node"
