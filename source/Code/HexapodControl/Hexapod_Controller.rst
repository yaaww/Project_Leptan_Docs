Main Loop
=========

This is the main hexapod controller, managing the timers and starting all required functions.

The main code runs the motion loop in a timer:

hexapod_controller.cpp:

.. code-block:: C
  :lineno-start: 99

    motion_control.update_vel(cmd_vel_);
    motion_control.motion_loop();
    motion_control.publish_joint_state(this->now());  // For RVIZ


The timer is started here:

.. code-block:: C
  :lineno-start: 99

    timer_ = this->create_wall_timer(10ms, std::bind(&HexapodController::timer_callback, this));


Create the main ROS node:

.. code-block:: C
  :lineno-start: 127

    auto hexa_node = std::make_shared<HexapodController>();


Subscribe to the joy pad input and start the node:

.. code-block:: C
  :lineno-start: 175

    auto subscription_ = hexa_node->create_subscription<sensor_msgs::msg::Joy>(
      "/joy", 1, joy_callback);

    spin(hexa_node);
