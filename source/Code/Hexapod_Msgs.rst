Hexapod Messages
=================

Custom messages defined for the Hexapod communication.


Feet Position
-------------

Position of all feet, consist of 6 times a Pose::

  hexapod_msgs/Pose[6] foot

Pose is used here instead of point because in the case of 4 joints the system is under-constraint
and requires additional information. This additional information in this case is the RPY of the foot.

Legs Joints
-----------

Groups 6 leg joints together to describe the whole robot::

  hexapod_msgs/LegJoints[6] leg


Leg Joints
----------

All joint angles of a leg::

  float64 coxa
  float64 femur
  float64 tibia
  float64 tarsus

The tarsus joint is not used if the robot only has 3 joints.


Pose
----

A pose consists of a point and an orientation::

  geometry_msgs/Point position
  hexapod_msgs/RPY orientation


RPY
---

Roll Pitch Yaw expressed as float64::

  float64 roll
  float64 pitch
  float64 yaw
