.. _HexapodControl:

Hexapod Control
===============

Main Directory, contains the movements and everything important.

Overview of the different files:

Hexapod Controller
------------------

Main file. Starts the node, adds the subscriber & publishers, initializes everything.

Details
~~~~~~~

.. toctree::
   :maxdepth: 2

   HexapodControl/Hexapod_Controller


Gait
----

This module generates the feet position sequence used for walking for each leg individually.

Details
~~~~~~~

.. toctree::
   :maxdepth: 2

   HexapodControl/Gait


Inverse Kinematics
------------------

Here is where the math works.

Inverse Kinematics takes the coordinates of a foot an translates it to servo angels, which are
required to reach this target point.

There are several explanations of how the math works online, a quick web search can help here.


Motion Control
--------------

Manages the different states like "Standing", "Walking", "Standing up", "Sitting down" etc.

Details
~~~~~~~

.. toctree::
   :maxdepth: 2

   HexapodControl/Motion_Control

Servo Driver
------------

Takes the joint angles as input, translates them into servo positions. Packages these positions and
forwards them to the servo motors through the Dynamixel SDK interface.
